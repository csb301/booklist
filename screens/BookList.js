import React, { useState } from 'react';
import { View, Text, FlatList, TouchableOpacity, Modal, Button, StyleSheet } from 'react-native';
import colors from '../color/color';

const BookList = ({ data }) => {
  const [selectedBook, setSelectedBook] = useState(null);

  const renderItem = ({ item }) => (
    <TouchableOpacity onPress={() => setSelectedBook(item)}>
      <View style={styles.bookItem}>
        <Text style={styles.title}>{item.title}</Text>
      </View>
    </TouchableOpacity>
  );

  return (
    <View style={styles.container}>
      <FlatList
        data={data}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
      />
      {selectedBook && (
        <Modal
          animationType="slide"
          transparent={false}
          visible={selectedBook !== null}
        >
          <View style={styles.modal}>
            <View style={styles.modalContent}>
              <Text style={styles.title}>{selectedBook.title}</Text>
              <Text style={styles.author}>{selectedBook.author}</Text>
              <Text style={styles.price}>{selectedBook.price}</Text>
              <Button title="Close" onPress={() => setSelectedBook(null)} />
            </View>
          </View>
        </Modal>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
    backgroundColor: colors.background,
  },
  bookItem: {
    padding: 16,
    borderBottomWidth: 1,
    borderBottomColor: colors.secondaryText,
    backgroundColor: colors.itemBackground,
    marginBottom: 10,
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
    color: colors.primaryText,
  },
  author: {
    fontSize: 14,
    color: colors.secondaryText,
  },
  price: {
    fontSize: 16,
    color: colors.priceColor,
  },
  modal: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.modalOverlay,
  },
  modalContent: {
    width: '80%',
    padding: 16,
    backgroundColor: 'white', // Set the background color to white
    borderRadius: 8,
  },
  closeButton: {
    alignSelf: 'flex-end',
    padding: 10,
  },
});

export default BookList;
