export default booksData = [
  {
    id: '1',
    title: 'DAWA',
    author: 'Kunzang Choden',
    price: 'Nu.450',
  },
  {
    id: '2',
    title: 'The Giver',
    author: 'Lois Lowry',
    price: 'Nu.280',
  },
  {
    id: '3',
    title: 'The Dream Bubia',
    author: 'Tempa Tshering',
    price: 'Nu.400',
  },
  {
    id: '4',
    title: 'The Empty Chair',
    author: 'Passang Dorji',
    price: 'Nu.580',
  },
  {
    id: '5',
    title: 'The Tourist Within',
    author: 'Denker gateway',
    price: 'Nu.300',
  },
  {
    id: '6',
    title: 'Merchant of venice',
    author: 'William Shakespare',
    price: 'Nu.280',
  },
  {
    id: '7',
    title: 'Gyelsay Leklen',
    author: 'Gyelsay Nechudhomay',
    price: 'Nu.280',
  },
  {
    id: '8',
    title: 'The Circle Of Karma',
    author: 'Kunzang Choden',
    price: 'Nu.450',
  },
  {
    id: '9',
    title: 'Lomba',
    author: 'Pema Euden',
    price: 'Nu.280',
  },
  {
    id: '10',
    title: 'Tales in color And Other Stories ',
    author: 'Kunzang Choden',
    price: 'Nu.430',
  },
  {
    id: '11',
    title: 'Butter Tea At Sunrise  ',
    author: 'Britta Das',
    price: 'Nu.100',
  },
  {
    id: '12',
    title: 'Khakey',
    author: 'Yeshi Tsheyang Zam',
    price: 'Nu.180',
  },
];