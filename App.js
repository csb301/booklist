import React from 'react';
import { SafeAreaView, StyleSheet } from 'react-native';
import BookList from './screens/BookList';
import booksData from './screens/Books'

const App = () => {
  return (
    <SafeAreaView style={styles.container}>
      <BookList data={booksData} />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default App;
