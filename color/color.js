export default colors = {
    primaryText: '#333', // Primary text color
    secondaryText: '#666', // Secondary text color
    background: '#F5F5F5', // Background color for the entire app
    itemBackground: 'white', // Background color for each book item
    priceColor: 'green', // Price text color
    modalOverlay: 'rgba(0, 0, 0, 0.7)', // Background color for the modal overlay
  };